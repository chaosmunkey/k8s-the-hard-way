#!/bin/bash

cat >/etc/hosts <<EOF

127.0.0.1    localhost localhost.localdomain localhost4 localhost4.localdomain4
::1          localhost localhost.localdomain localhost4 localhost6.localdomain6

192.168.255.1     k8s-master-1 k8s-master-1.example.com
192.168.255.2     k8s-master-2 k8s-master-2.example.com

192.168.255.11    k8s-worker-1 k8s-worker-1.example.com
192.168.255.12    k8s-worker-2 k8s-worker-2.example.com

EOF

