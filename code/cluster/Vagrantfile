# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do | config |

  config.vm.box = "bento/centos-7.6"
  config.vm.box_version = "201812.27.0"

  # config.vm.provider "virtualbox" do |v|
  #   v.default_nic_type = "82540EM"
  # end

  config.vm.provision :shell, path: "provision.sh"

  domain = ENV['DOMAIN'] || "example.com"
  num_lbs = ENV['NUM_LBS'].to_i > 0 ? ENV['NUM_LBS'].to_i : 1

  (1..num_lbs).each do | i |

    lb_name = "k8s-lb-#{i}"
    config.vm.define lb_name do | lb |

      lb.vm.provider :virtualbox do | v |
        v.name = lb_name
        v.memory = 1024
        v.cpus = 1
      end

      lb.vm.hostname = "#{lb_name}.#{domain}"

      # Host-only adapter: will need to set a route on local machine to access Internal Network
      lb.vm.network "private_network", ip: "192.168.254.#{200 + i}"
      # Internal Network adaptor
      lb.vm.network "private_network", ip: "192.168.255.#{255 - i}", virtualbox__intnet: "k8s"

      lb.vm.provision :ansible do | ansible |
        ansible.inventory_path = "hosts"
        ansible.playbook = "load_balancer.yml"
      end
    end

  end

  (1..2).each do | i |
    master_name = "k8s-master-#{i}"

    config.vm.define master_name do | master |

      master.vm.provider :virtualbox do | v |
        v.name = master_name
        v.memory = 2048
        v.cpus = 2
      end

      master.vm.hostname = "#{master_name}.#{domain}"

      master.vm.network "private_network", :mac => "080027A8FF0#{i}", virtualbox__intnet: "k8s", type: "dhcp"

      master.vm.provision :ansible do | ansible |
        ansible.playbook = "k8s.yml"
        ansible.vault_password_file = "./vault.pass"
        ansible.groups = {
          "k8s:children" => ["k8s_masters"],
          "k8s_masters" => [master_name]
        }
      end
    end
  end

  (1..2).each do | i |
    worker_name = "k8s-worker-#{i}"

    config.vm.define worker_name do | worker |

      worker.vm.provider :virtualbox do | v |
        v.name = worker_name
        v.memory = 1024
        v.cpus = 1
      end

      worker.vm.hostname = "#{worker_name}.#{domain}"

      worker.vm.network "private_network", :mac => "080027A8FF1#{i}", virtualbox__intnet: "k8s", type: "dhcp"

      worker.vm.provision :ansible do | ansible |
        ansible.playbook = "k8s.yml"
        ansible.vault_password_file = "./vault.pass"
        ansible.groups = {
          "k8s:children" => ["k8s_workers"],
          "k8s_workers" => [worker_name]
        }
      end
    end
  end

end
