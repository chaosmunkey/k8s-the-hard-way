#!/bin/bash

source vars.sh

COMMON_CERTS=(ca.pem)
MASTER_CERTS=(ca-key.pem admin.pem admin-key.pem kube-controller-manager.pem kube-controller-manager-key.pem kube-proxy.pem kube-proxy-key.pem kube-scheduler.pem kube-scheduler-key.pem kubernetes.pem kubernetes-key.pem service-account.pem service-account-key.pem)


while getopts ":e" arg;
do
    case "${arg}" in
        e)
            ENCRYPT=true
        ;;
        *)
        ;;
    esac
done


if [ "${ENCRYPT}" = "true" ];
then
    read -rsp "Vault Password: " PASSWORD
    echo
fi


function insert_into_vault {
    VAULT=$1
    CERT=$2
    # VAULT_ITEM=$(echo "${CERT}" | sed -e 's/[.-]/_/g') # -> https://www.shellcheck.net/wiki/SC2001
    # VAULT_ITEM=$(echo "${CERT//[.-]/_}")               # -> https://www.shellcheck.net/wiki/SC2116
    if [ -z "$3" ];
    then
        VAULT_ITEM="${CERT//[.-]/_}"
    else
        VAULT_ITEM="$3"
    fi

    if [ ! -f "${CERT}" ];
    then
        echo "${CERT} does not exist".
        return
    fi

    echo "${VAULT_ITEM}: |" >> "${VAULT}"
    while read -r LINE
    do
        echo "  ${LINE}" >> "${VAULT}"
    done < "${CERT}"

    echo -e "\n\n" >> "${VAULT}"
}


CWD=$(pwd)

if [ ! -d "${OUTPUT_DIR}" ];
then
    mkdir -p "${OUTPUT_DIR}" || exit
fi
cd "${OUTPUT_DIR}" || exit


echo "Generating COMMON_CERTS vault..."
if [ -f COMMON_CERTS ];
then
    rm COMMON_CERTS
fi

for cert in "${COMMON_CERTS[@]}"
do
    insert_into_vault "COMMON_CERTS" "${cert}"
done


echo "Generating MASTER_CERTS vault..."
if [ -f MASTER_CERTS ];
then
    rm MASTER_CERTS
fi

for cert in "${MASTER_CERTS[@]}"
do
    insert_into_vault "MASTER_CERTS" "${cert}"
done


echo "Generating WORKER_CERT vaults..."
for i in $(seq 1 $NUM_WORKERS);
do
    HOSTNAME="${WORKER_HOSTNAME_PREFIX}${i}.${DOMAIN}"
    VAULT_NAME=$(echo "${HOSTNAME}_CERTS" | tr '[:lower:]' '[:upper:]')

    if [ -f "${VAULT_NAME}" ];
    then
        rm "${VAULT_NAME}"
    fi

    insert_into_vault "$VAULT_NAME" "${HOSTNAME}.pem" "kubelet_pem"
    insert_into_vault "$VAULT_NAME" "${HOSTNAME}-key.pem" "kubelet_key_pem"
done


if [ "${ENCRYPT}" ];
then
    TEMP_FILE=$(mktemp)
    echo "${PASSWORD}" > "${TEMP_FILE}"
    ansible-vault encrypt ./*_CERTS --vault-password-file "${TEMP_FILE}"
    rm -r "${TEMP_FILE}"
fi


cd "${CWD}" || exit
