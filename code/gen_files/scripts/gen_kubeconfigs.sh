#!/bin/bash

source vars.sh

echo "#################################################"
echo "#                    KUBELET                    #"
echo "#################################################"
for i in $(seq 1 $NUM_WORKERS); do

    node="${WORKER_HOSTNAME_PREFIX}-${i}.${DOMAIN}"
    kubectl config set-cluster kubernetes-the-hard-way \
        --certificate-authority=ca.pem \
        --embed-certs=true \
        --server=https://${LOAD_BALANCER_IP}:6443 \
        --kubeconfig=${node}.kubeconfig

    kubectl config set-credentials system:node:${node} \
        --client-certificate=${node}.pem \
        --client-key=${node}-key.pem \
        --embed-certs=true \
        --kubeconfig=${node}.kubeconfig

    kubectl config set-context default \
        --cluster=kubernetes-the-hard-way \
        --user=system:node:${node} \
        --kubeconfig=${node}.kubeconfig

    kubectl config use-context default --kubeconfig=${node}.kubeconfig
done
