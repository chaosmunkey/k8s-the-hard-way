#!/bin/bash


CWD=$(pwd)
cd ../output


echo "#################################################"
echo "#                 CA AUTHORITY                  #"
echo "#################################################"

cat > "ca-config.json" <<EOF
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "8760h"
      }
    }
  }
}
EOF

cat > "ca-csr.json" <<EOF
{
  "CN": "Kubernetes",
  "hosts": [
    "k8s-master-1.example.com",
    "192.168.255.1",
    "k8s-master-2.example.com",
    "192.168.255.2"
  ],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "UK",
      "L": "Portsmouth",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Hants"
    }
  ]
}
EOF

cfssl gencert -initca "ca-csr.json" | cfssljson -bare ca -stdout


echo "#################################################"
echo "#                     ADMIN                     #"
echo "#################################################"

cat > admin-csr.json <<EOF
{
  "CN": "admin",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "UK",
      "L": "Portsmouth",
      "ST": "Hants",
      "O": "system:masters",
      "OU": "Kubernetes The Hard Way"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  -hostname=k8s-master-1.example.com,192.168.255.1,k8s-master-2.example.com,192.168.255.2 \
  admin-csr.json | cfssljson -bare admin


echo "#################################################"
echo "#                    KUBELET                    #"
echo "#################################################"

for i in {1..2}; do

HOSTNAME="k8s-worker-${i}.example.com"

cat > ${HOSTNAME}-csr.json <<EOF
{
  "CN": "system:node:",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "UK",
      "L": "Portsmouth",
      "ST": "Hants",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way"
    }
  ]
}
EOF

IP="192.168.255.1${i}"

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=${HOSTNAME},${IP} \
  -profile=kubernetes \
  ${HOSTNAME}-csr.json | cfssljson -bare ${HOSTNAME}
done

echo "#################################################"
echo "#               CONTROLLER MANAGER              #"
echo "#################################################"

cat > kube-controller-manager-csr.json <<EOF
{
  "CN": "system:kube-controller-manager",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "UK",
      "L": "Portsmouth",
      "ST": "Hants",
      "O": "system:kube-controller-manager",
      "OU": "Kubernetes The Hard Way"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  -hostname=k8s-master-1.example.com,192.168.255.1,k8s-master-2.example.com,192.168.255.2 \
  kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager


echo "#################################################"
echo "#                   KUBE PROXY                  #"
echo "#################################################"

cat > kube-proxy-csr.json <<EOF
{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "UK",
      "L": "Portsmouth",
      "ST": "Hants",
      "O": "system:node-proxier",
      "OU": "Kubernetes The Hard Way"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  -hostname=k8s-worker-1.example.com,192.168.255.11,k8s-worker-2.example.com,192.168.255.12 \
  kube-proxy-csr.json | cfssljson -bare kube-proxy


echo "#################################################"
echo "#                 KUBE SCHEDULER                #"
echo "#################################################"

cat > kube-scheduler-csr.json <<EOF
{
  "CN": "system:kube-scheduler",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "UK",
      "L": "Portsmouth",
      "ST": "Hants",
      "O": "system:kube-scheduler",
      "OU": "Kubernetes The Hard Way"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  kube-scheduler-csr.json | cfssljson -bare kube-scheduler


echo "#################################################"
echo "#                   KUBE API                    #"
echo "#################################################"

CERT_HOSTNAME=10.32.0.1,192.168.255.1,k8s-master-1.example.com,192.168.255.2,k8s-master-2.example.com,192.168.255.254,k8s-lb-1.example.com,k8s-lb-2.example.com,127.0.0.1,localhost,kubernetes.default

cat > kubernetes-csr.json <<EOF
{
  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "UK",
      "L": "Portsmouth",
      "ST": "Hants",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=${CERT_HOSTNAME} \
  -profile=kubernetes \
  kubernetes-csr.json | cfssljson -bare kubernetes


echo "#################################################"
echo "#                SERVICE ACCOUNT                #"
echo "#################################################"

cat > service-account-csr.json <<EOF
{
  "CN": "service-accounts",
  "hosts": [
    "k8s-master-1.example.com",
    "k8s-master-2.example.com"
  ],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "UK",
      "L": "Portsmouth",
      "ST": "Hants",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  service-account-csr.json | cfssljson -bare service-account


cd "${CWD}"
