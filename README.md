# Kubernetes - The Hard Way

This project follows [_Kelsey Hightower's_](https://github.com/kelseyhightower/kubernetes-the-hard-way) project of setting up a _`Kubernetes`_ cluster from scratch. However, instead of using _GCP_, I've decided I'd have a go at building a cluster locally using VMs so that I am able to spin out a test cluster with in minutes (hopefully).


### Tools
Some of the tools I aim to use for this project to provision the cluster are:
* `VirtualBox`
* `Ansible` and/or `Chef`
