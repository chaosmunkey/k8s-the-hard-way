# Bootstrapping `etcd` Cluster

`etcd` is a stateless datastore which is used for storing the state of the _Kubernetes_ cluster.

## Bootstrapping
Commands need to be run on all of the _master_ nodes.

### Download and Install the Binaries
```
wget -q --show-progress --https-only --timestamping "https://github.com/coreos/etcd/releases/download/v3.3.9/etcd-v3.3.9-linux-amd64.tar.gz"

tar -xvf etcd-v3.3.9-linux-amd64.tar.gz
mv etcd-v3.3.9-linux-amd64/etcd* /usr/local/bin/
```

### Configure _etcd_ server
```
mkdir -p /etc/etcd /var/lib/etcd
cp /etc/kubernetes/{ca.pem,kubernetes-key.pem,kubernetes.pem} /etc/etcd/
```

```
ETCD_NAME=$(hostname)
INTERNAL_IP=$()
INITIAL_CLUSTER=k8s-master-1.example.com=https://192.168.255.1:2380,k8s-master-2.example.com=https://192.168.255.2:2380

cat <<EOF | sudo tee /etc/systemd/system/etcd.service
[Unit]
Description=etcd
Documentation=https://github.com/coreos

[Service]
ExecStart=/usr/local/bin/etcd \\
  --name ${ETCD_NAME} \\
  --cert-file=/etc/etcd/kubernetes.pem \\
  --key-file=/etc/etcd/kubernetes-key.pem \\
  --peer-cert-file=/etc/etcd/kubernetes.pem \\
  --peer-key-file=/etc/etcd/kubernetes-key.pem \\
  --trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-client-cert-auth \\
  --client-cert-auth \\
  --initial-advertise-peer-urls https://${INTERNAL_IP}:2380 \\
  --listen-peer-urls https://${INTERNAL_IP}:2380 \\
  --listen-client-urls https://${INTERNAL_IP}:2379,https://127.0.0.1:2379 \\
  --advertise-client-urls https://${INTERNAL_IP}:2379 \\
  --initial-cluster-token etcd-cluster-0 \\
  --initial-cluster ${INITIAL_CLUSTER} \\
  --initial-cluster-state new \\
  --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```

### Start _etcd_
```
sudo systemctl daemon-reload
sudo systemctl enable --now etcd
```

### Verification
```
sudo ETCDCTL_API=3 /usr/local/bin/etcdctl member list \
  --endpoints=https://127.0.0.1:2379 \
  --cacert=/etc/etcd/ca.pem \
  --cert=/etc/etcd/kubernetes.pem \
  --key=/etc/etcd/kubernetes-key.pem
```


## Troubleshooting
### `missing '='`
Whilst following this tutorial (and attempting to create the _Ansible_ role), I found the _etcd_ service was unable to start and would throw out the following error:

> [/etc/systemd/system/etcd.service:7] Missing '='.


I attempted to use _journalctl_ to see if there was anymore info available, but that turned up no results.

Looking at the message, it looks as though something on **line 7** was to blame.

The error was related to having `\\` in the `etcd.service` file instead of a single `\`. Once replaced with the correct number of backslashes, the service started up normally.


### `ServerName ""`
Another problem I encountered with this tutorial was when the _etcd_ service started. When attempting to verify the _etcd_ cluster. I would recieve the following error:
> Error: context deadline exceeded

When looking at the output of the status of the _etcd_ service, I saw a large number of the errors below:

> rejected connection from "192.168.255.2:50310" (error "remote error: tls: bad certificate", ServerName "")

The cause of this was due to some of the certificates which were generated were missing the hostname/IP address of the server. This was easily remedied by regenerating the certificates with the hostname and IP addresses correctly set.
