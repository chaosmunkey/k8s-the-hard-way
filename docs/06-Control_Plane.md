# Control Plane

## What is the Control Plane?

The control plane is a set of services which control the _Kubernetes_ cluster.

Components of the control plane make decisions about the cluster, and detect and respond to events within the cluster... e.g. Scheduling a new pod if the spec of a _ReplicaSet_ isn't met.


## Components of the Control Plane
|Component|Description|
|:-------:|:---------:|
|**kube-apiserver**|Serves the _Kubernetes_ API; allows users to interact with the cluster. Is the interface to the _Control Plane_.|
|**etcd**|_Kubernetes_ cluster datastore (Open Source third party tool).|
|**kube-scheduler**|Schedules pods on available worker nodes.|
|**kube-controller-manager**|Runs a series of controllers that provide a wide range of functionality; collects many smaller services and allows them to all be controlled by a single service.|
|**cloud-controller-manager**|Handles interactions and integration with cloud providers.|


## Installing Control Plane Binaries
As a pre-step, create the config directory.
```
sudo mkdir -p /etc/kubernetes/config
```


### Download and Install Kubernetes Binaries
```
wget -q --show-progress --https-only --timestamping \
  "https://storage.googleapis.com/kubernetes-release/release/v1.14.3/bin/linux/amd64/kube-apiserver" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.14.3/bin/linux/amd64/kube-controller-manager" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.14.3/bin/linux/amd64/kube-scheduler" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.14.3/bin/linux/amd64/kubectl"

chmod +x kube-apiserver kube-controller-manager kube-scheduler kubectl
sudo mv kube-apiserver kube-controller-manager kube-scheduler kubectl /usr/local/bin/
```


### Setting up the Kubernetes API Server
Move the PKI keys and _kubeconfigs_ generated in the previous steps to the necessary location.
```
sudo mkdir -p /var/lib/kubernetes

sudo mv ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem \
    service-account-key.pem service-account.pem \
    encryption-config.yaml /var/lib/kubernetes/
```

Set a few environmental variables:
```
INTERNAL_IP=$(some command)
CONTROLLER0=https://192.168.255.1:2379
CONTROLLER1=https://192.168.255.2:2379
```

Create the __systemd__ unit file for the API server:
```
cat <<EOF | sudo tee /etc/systemd/system/kube-apiserver.service
[Unit]
Description=Kubernetes API Server
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-apiserver \\
  --advertise-address=${INTERNAL_IP} \\
  --allow-privileged=true \\
  --apiserver-count=3 \\
  --audit-log-maxage=30 \\
  --audit-log-maxbackup=3 \\
  --audit-log-maxsize=100 \\
  --audit-log-path=/var/log/audit.log \\
  --authorization-mode=Node,RBAC \\
  --bind-address=0.0.0.0 \\
  --client-ca-file=/var/lib/kubernetes/ca.pem \\
  --enable-admission-plugins=Initializers,NamespaceLifecycle,NodeRestriction,LimitRanger,ServiceAccount,DefaultStorageClass,ResourceQuota \\
  --enable-swagger-ui=true \\
  --etcd-cafile=/var/lib/kubernetes/ca.pem \\
  --etcd-certfile=/var/lib/kubernetes/kubernetes.pem \\
  --etcd-keyfile=/var/lib/kubernetes/kubernetes-key.pem \\
  --etcd-servers=${CONTROLLER0},${CONTOLLER1} \\
  --event-ttl=1h \\
  --experimental-encryption-provider-config=/var/lib/kubernetes/encryption-config.yaml \\
  --kubelet-certificate-authority=/var/lib/kubernetes/ca.pem \\
  --kubelet-client-certificate=/var/lib/kubernetes/kubernetes.pem \\
  --kubelet-client-key=/var/lib/kubernetes/kubernetes-key.pem \\
  --kubelet-https=true \\
  --runtime-config=api/all \\
  --service-account-key-file=/var/lib/kubernetes/service-account.pem \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --service-node-port-range=30000-32767 \\
  --tls-cert-file=/var/lib/kubernetes/kubernetes.pem \\
  --tls-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \\
  --v=2 \\
  --kubelet-preferred-address-types=InternalIP,InternalDNS,Hostname,ExternalIP,ExternalDNS
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```


### Setting Up the Kubernetes Controller Manager
First we need to move the _kube-controller-manager.kubeconfig_ file previously generated to the necessary location:
```
sudo mv kube-controller-manager.kubeconfig /var/lib/kubernetes/
```

Create the __systemd__ unit file for the controller manager:
```
cat <<EOF | sudo tee /etc/systemd/system/kube-controller-manager.service
[Unit]
Description=Kubernetes Controller Manager
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-controller-manager \\
  --address=0.0.0.0 \\
  --cluster-cidr=10.200.0.0/16 \\
  --cluster-name=kubernetes \\
  --cluster-signing-cert-file=/var/lib/kubernetes/ca.pem \\
  --cluster-signing-key-file=/var/lib/kubernetes/ca-key.pem \\
  --kubeconfig=/var/lib/kubernetes/kube-controller-manager.kubeconfig \\
  --leader-elect=true \\
  --root-ca-file=/var/lib/kubernetes/ca.pem \\
  --service-account-private-key-file=/var/lib/kubernetes/service-account-key.pem \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --use-service-account-credentials=true \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```


### Setting Up the Kubernetes Scheduler
First we need to move the _kube-scheduler.kubeconfig_ file previously generated to the necessary location:
```
sudo mv kube-scheduler.kubeconfig /var/lib/kubernetes/
```

Create a __yaml__ file for the _Kubernetes Scheduler_:
```
cat <<EOF | sudo tee /etc/kubernetes/config/kube-scheduler.yaml
apiVersion: componentconfig/v1alpha1
kind: KubeSchedulerConfiguration
clientConnection:
  kubeconfig: "/var/lib/kubernetes/kube-scheduler.kubeconfig"
leaderElection:
  leaderElect: true
EOF
```

Create the __systemd__ unit file for the scheduler:
```
cat <<EOF | sudo tee /etc/systemd/system/kube-scheduler.service
[Unit]
Description=Kubernetes Scheduler
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-scheduler \\
  --config=/etc/kubernetes/config/kube-scheduler.yaml \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
```


### Start the Controller Services
```
sudo systemctl daemon-reload
sudo systemctl enable --now  kube-apiserver kube-controller-manager kube-scheduler
```

#### Verify Services Started Correctly
```
sudo systemctl status kube-apiserver kube-controller-manager kube-scheduler

/usr/local/bin/kubectl get componentstatuses --kubeconfig /var/lib/kubernetes/admin.kubeconfig
```
> Output:
```
NAME                 STATUS    MESSAGE              ERROR
controller-manager   Healthy   ok
scheduler            Healthy   ok
etcd-0               Healthy   {"health": "true"}
etcd-1               Healthy   {"health": "true"}
```


### Enabling HTTP Health Checks
In the original guide, Kelsey Hightower used a GCP load balancer. The load balancer needs to be able to perform health checks against the _Kubernetes API_ to check the health status of each of the API nodes.

Unfortunately, there isn't an easy way to retrieve the status over HTTPS, so Kelsey set up a proxy server to allow these health checks to be performed over HTTP.

With the way I have set up my cluster, this isn't a necessary step. However, I will be following this as it's a useful exercise.

#### Health End Point
On any of the controller nodes, the following URI can be accessed to give the status of a given node:
```
https://localhost:6443/healthz
```

>Output:
`ok`

Following commands can be run to retrieve this:

```
curl --cacert /var/lib/kubernetes/ca.pem https://localhost:6443/healthz
curl -k https://localhost:6443/healthz
```

#### Install and setup `nginx` on Controller Nodes
My infrastructure is built using __CentOS__, so this step is slightly different to the guide which uses __Ubuntu__.

```
sudo yum install epel-release
sudo yum install nginx

cat > kubernetes.default.svc.cluster.local.conf <<EOF
server {
  listen      80;
  server_name kubernetes.default.svc.cluster.local;

  location /healthz {
     proxy_pass                    https://127.0.0.1:6443/healthz;
     proxy_ssl_trusted_certificate /var/lib/kubernetes/ca.pem;
  }
}
EOF

sudo mv kubernetes.default.svc.cluster.local.conf /etc/nginx/conf.d/

sudo systemctl restart nginx
sudo systemctl enable nginx

curl -H "Host: kubernetes.default.svc.cluster.local" -i http://127.0.0.1/healthz
```


### Set up RBAC for Kubelet Authentication
RBAC stands for **R**ole **B**ased **A**ccess **C**ontrol, which is simply a way within _Kubernetes_ to create roles and assign permissions to different users.

This particular step is only required to be run on one of the master nodes; it shouldn't break anything if it's run on all.

This step creates the `system:kube-apiserver-to-kubelet` _ClusterRole_ which grants permissions to allow the _Kubelet API_ to be accessed as well as to be able to perform common tasks such as managing _pods_.

```
cat <<EOF | /usr/local/bin/kubectl apply --kubeconfig /var/lib/kubernetes/admin.kubeconfig -f -
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "true"
  labels:
    kubernetes.io/bootstrapping: rbac-defaults
  name: system:kube-apiserver-to-kubelet
rules:
  - apiGroups:
      - ""
    resources:
      - nodes/proxy
      - nodes/stats
      - nodes/log
      - nodes/spec
      - nodes/metrics
    verbs:
      - "*"
EOF
```

In order to use the role, it needs to be assigned to a user; in this instance, the _kubernetes_ user.

```
cat <<EOF | /usr/local/bin/kubectl apply --kubeconfig /var/lib/kubernetes/admin.kubeconfig -f -
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: system:kube-apiserver
  namespace: ""
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:kube-apiserver-to-kubelet
subjects:
  - apiGroup: rbac.authorization.k8s.io
    kind: User
    name: kubernetes
EOF
```

### Setting up a Kube API Frontend Load Balancer
In this step, I set up the frontend load balancer. In the original documentation this is set up using a node in GCP. As I'm not using GCP to build my cluster, I will need to create my own machine for this purpose; this will be the same machine I'm using as my __DHCP__ and __DNS__ server.

There are a number of pieces of software which can be used for this purpose; **nginx** and **haproxy**. In this example, I shall be focusing on **nginx**, but I might write up how to do the same with **haproxy** at a later date.
```
sudo yum install -y nginx

sudo mkdir -p /etc/nginx/tcpconf.d/
```

The following line needs to be added to the `/etc/nginx/nginx.conf` file:
```
include /etc/nginx/tcpconf.d/*
```

Now to create the configuration file for the load balancer:
```
CONTROLLER0_IP=192.168.255.1
CONTROLLER1_IP=192.168.255.2

cat << EOF | sudo tee /etc/nginc/tcpconf.d/kubernetes.conf
stream {
      upstream kubernetes {
          server $CONTROLLER0_IP:6443;
          server $CONTROLLER1_IP:6443;
      }

      server {
          listen 6443;
          listen 443;
          proxy_pass kubernetes;
      }
}
EOF

# Reload changes - make nginx pick up the new changes.
sudo nginx -s reload
```


```
sudo systemctl enable nginx
sudo systemctl start nginx
```
