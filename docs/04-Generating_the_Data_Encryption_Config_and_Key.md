# Generating the Data Encryption Config and Key

Kubernetes has the ability to encrypt secrets; any sensitive data isn't stored on the disk in plain text. In order to make use of this feature, we need to pass an encryption key to Kubernetes so that it can encrypt and decrypt said sensitive data.

## Generate Encryption Key
This key will be included in the _encryption config file_.

```
ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)
```

## The Encryption Config File
```
cat > encryption-config.yaml <<EOF
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: ${ENCRYPTION_KEY}
      - identity: {}
EOF
```
