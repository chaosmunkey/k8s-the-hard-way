# Prerequisites and Tools

## Infrastructure
### VirtualBox
The plan is to role out the cluster on "bare metal", and seeing as I don't have number of physical machines laying about, the next best thing is to virtualise the infrastructure.

Latest version of _VirtualBox_ can be downloaded from here:
https://www.virtualbox.org/wiki/Downloads

> Other hypervisors are available...

### Ansible
_Ansible_ will be used to automate the creation of the _Kubernetes_ cluster.

The _playbooks_ created in this repo don't depend on any particular version of _Ansible_, to the latest and greatest can be used. To install the latest version, run the following command (providing _Python_ and _pip_ are both installed on the system):
```
pip install -U ansible
```

## Client Tools
### `cfssl`
_cfssl_ and _cfssljson_ are command line tools which will be used to generate the _PKI (**P**ublic **K**ey **I**nfrastructure)_ and TLS certificates.

#### Install
```
wget -q --show-progress --https-only --timestamping \
  https://pkg.cfssl.org/R1.2/cfssl_linux-amd64 \
  https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64

chmod +x cfssl_linux-amd64 cfssljson_linux-amd64
sudo mv cfssl_linux-amd64 /usr/local/bin/cfssl
sudo mv cfssljson_linux-amd64 /usr/local/bin/cfssljson
```

#### Verification
Verify cfssl version 1.2.0 or higher is installed:

```
cfssl version
```
> output
```
Version: 1.2.0
Revision: dev
Runtime: go1.6
```

### `kubectl`
_kubectl_ is a command line utility for interacting with a _Kubernetes_ API server.

#### Install
```
wget https://storage.googleapis.com/kubernetes-release/release/v1.14.3/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mv kubectl /usr/local/bin/
```

#### Verification
```
kubectl version --client
```
>Output
```
Client Version: version.Info{Major:"1", Minor:"14", GitVersion:"v1.14.3", GitCommit:"5e53fd6bc17c0dec8434817e69b04a25d8ae0ff0", GitTreeState:"clean", BuildDate:"2019-06-06T01:44:30Z", GoVersion:"go1.12.5", Compiler:"gc", Platform:"darwin/amd64"}
```
